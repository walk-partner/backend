package com.as.app.adapter.web.resources;

import com.as.api.PetOwnerApi;
import com.as.app.adapter.web.dtos.PetOwnerDto;
import com.as.model.PetOwner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/pet-owners")
@RestController
public class PetOwnerResource {
    private final PetOwnerApi petOwnerApi;

    public PetOwnerResource(PetOwnerApi petOwnerApi) {
        this.petOwnerApi = petOwnerApi;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PetOwnerDto> getPetOwner(@PathVariable Long id) {
        PetOwner petOwner = petOwnerApi.getPetOwner(id);
        return ResponseEntity.ok(PetOwnerDto.of(petOwner));
    }
    @PostMapping
    public ResponseEntity<PetOwnerDto> createPetOwner(@RequestBody PetOwnerDto petOwnerDto) {
        PetOwner petOwner = petOwnerApi.createPetOwner(PetOwnerDto.newPetOwner(petOwnerDto, null));
        return ResponseEntity.ok(PetOwnerDto.of(petOwner));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PetOwnerDto> updatePetOwner(@PathVariable Long id, @RequestBody PetOwnerDto petOwnerDto) {
        PetOwner petOwner = petOwnerApi.updatePetOwner(PetOwnerDto.newPetOwner(petOwnerDto, id));
        return ResponseEntity.ok(PetOwnerDto.of(petOwner));
    }
}
