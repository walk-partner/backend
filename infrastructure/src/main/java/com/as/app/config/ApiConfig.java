package com.as.app.config;

import com.as.api.PetOwnerApi;
import com.as.api.PetOwnerService;
import com.as.spi.PetOwnerSpi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiConfig {
    @Bean
    public PetOwnerApi petOwnerApi(PetOwnerSpi petOwnerSpi) {
        return new PetOwnerService(petOwnerSpi);
    }
}
