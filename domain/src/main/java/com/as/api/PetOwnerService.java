package com.as.api;

import com.as.model.PetOwner;
import com.as.spi.PetOwnerSpi;

public class PetOwnerService implements PetOwnerApi {
    private final PetOwnerSpi petOwnerSpi;

    public PetOwnerService(PetOwnerSpi petOwnerSpi) {
        this.petOwnerSpi = petOwnerSpi;
    }


    @Override
    public PetOwner createPetOwner(PetOwner petOwner) {
        return petOwnerSpi.createPetOwner(petOwner);
    }

    @Override
    public PetOwner updatePetOwner(PetOwner petOwner) {
        return petOwnerSpi.updatePetOwner(petOwner);

    }

    @Override
    public void deletePetOwner(Long id) {
        petOwnerSpi.deletePetOwner(id);
    }

    @Override
    public PetOwner getPetOwner(Long id) {
        return petOwnerSpi.getPetOwner(id);
    }
}
