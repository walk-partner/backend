package com.as.app.adapter.persistence;

import com.as.model.PetOwner;
import com.as.spi.PetOwnerSpi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.Objects;

@Repository
public class PetOwnerPersistenceAdapter implements PetOwnerSpi {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PetOwnerPersistenceAdapter(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public PetOwner createPetOwner(PetOwner petOwner) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO pet_owner (first_name, last_name, address, phone, email) VALUES (?, ?, ?, ?, ?)", new String[]{"id"});
            preparedStatement.setString(1, petOwner.firstName());
            preparedStatement.setString(2, petOwner.lastName());
            preparedStatement.setString(3, petOwner.address());
            preparedStatement.setString(4, petOwner.phone());
            preparedStatement.setString(5, petOwner.email());
            return preparedStatement;
        }, generatedKeyHolder);
        if (Objects.nonNull(generatedKeyHolder.getKey())) {
            return PetOwner.builder()
                    .id(generatedKeyHolder.getKey().longValue())
                    .firstName(petOwner.firstName())
                    .lastName(petOwner.lastName())
                    .address(petOwner.address())
                    .phone(petOwner.phone())
                    .email(petOwner.email())
                    .build();
        }
        throw new RuntimeException("Could not create pet owner");
    }

    @Override
    public PetOwner updatePetOwner(PetOwner petOwner) {
        int update = jdbcTemplate.update("UPDATE pet_owner SET first_name = ?, last_name = ?, address = ?, phone = ?, email = ? WHERE id = ?", petOwner.firstName(), petOwner.lastName(), petOwner.address(), petOwner.phone(), petOwner.email(), petOwner.id());
        if (update == 1) {
            return petOwner;
        }
        throw new RuntimeException("Pet owner not found");
    }

    @Override
    public void deletePetOwner(Long id) {
        jdbcTemplate.update("DELETE FROM pet_owner WHERE id = ?", id);
    }

    @Override
    public PetOwner getPetOwner(Long id) {
        String selectQuery = "SELECT id, first_name, last_name, address, phone, email FROM pet_owner WHERE id = ?";
        return jdbcTemplate.query(selectQuery, getPetOwnerResultSetExtractor(), id);
    }

    private ResultSetExtractor<PetOwner> getPetOwnerResultSetExtractor() {
        return rs -> {
            if (rs.next()) {
                return PetOwner.builder()
                        .id(rs.getLong("id"))
                        .firstName(rs.getString("first_name"))
                        .lastName(rs.getString("last_name"))
                        .address(rs.getString("address"))
                        .phone(rs.getString("phone"))
                        .email(rs.getString("email"))
                        .build();
            }
            return null;
        };
    }
}
