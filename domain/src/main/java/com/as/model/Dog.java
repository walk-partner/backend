package com.as.model;

public class Dog {
    public enum Size {
        SMALL, MEDIUM, LARGE
    }
    public Long id;
    public String name;
    public String breed;
    public String color;
    public Size size;
    public String  description;
    public PetOwner owner;
}
