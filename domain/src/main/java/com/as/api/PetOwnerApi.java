package com.as.api;

import com.as.model.PetOwner;

public interface PetOwnerApi {
    PetOwner createPetOwner(PetOwner petOwner);

    PetOwner updatePetOwner(PetOwner petOwner);

    void deletePetOwner(Long id);

    PetOwner getPetOwner(Long id);
}
