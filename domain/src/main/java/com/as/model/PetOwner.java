package com.as.model;


import lombok.Builder;

@Builder
public record PetOwner(Long id, String firstName, String lastName, String address, String phone, String email) {
}
