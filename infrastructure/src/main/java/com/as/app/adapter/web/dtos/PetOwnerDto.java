package com.as.app.adapter.web.dtos;

import com.as.model.PetOwner;

public record PetOwnerDto(String firstName,String lastName, String address, String phone, String email) {
    public static PetOwnerDto of(PetOwner petOwner) {
        return new PetOwnerDto(petOwner.firstName(), petOwner.lastName(), petOwner.address(), petOwner.phone(), petOwner.email());
    }

    public static PetOwner newPetOwner(PetOwnerDto petOwnerDto, Long id) {
        return PetOwner.builder()
                .id(id)
                .firstName(petOwnerDto.firstName)
                .lastName(petOwnerDto.lastName)
                .address(petOwnerDto.address)
                .phone(petOwnerDto.phone)
                .email(petOwnerDto.email)
                .build();
    }
}
