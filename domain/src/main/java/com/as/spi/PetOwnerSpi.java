package com.as.spi;

import com.as.model.PetOwner;

public interface PetOwnerSpi {
    PetOwner createPetOwner(PetOwner petOwner);

    PetOwner updatePetOwner(PetOwner petOwner);

    void deletePetOwner(Long id);

    PetOwner getPetOwner(Long id);
}
